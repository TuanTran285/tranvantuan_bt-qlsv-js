function SinhVien(_maSv, _hoTen, _email, _matKhau, _diemToan, _diemLy, _diemHoa) {
    this.maSv = _maSv
    this.hoTen = _hoTen
    this.email = _email
    this.matKhau = _matKhau
    this.diemToan = _diemToan
    this.diemLy = _diemLy
    this.diemHoa = _diemHoa
    this.tinhDTB = function() {
        return ((this.diemToan + this.diemHoa + this.diemLy) / 3).toFixed(1)
    }
}