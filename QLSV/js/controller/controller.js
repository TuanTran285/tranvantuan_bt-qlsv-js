function layThongTinTuForm() {
  const _maSv = document.getElementById("txtMaSV").value;
  const _hoTen = document.getElementById("txtTenSV").value;
  const _email = document.getElementById("txtEmail").value;
  const _matKhau = document.getElementById("txtPass").value;
  const _diemToan = document.getElementById("txtDiemToan").value * 1;
  const _diemLy = document.getElementById("txtDiemLy").value * 1;
  const _diemHoa = document.getElementById("txtDiemHoa").value * 1;

  const sv = new SinhVien(
    _maSv,
    _hoTen,
    _email,
    _matKhau,
    _diemToan,
    _diemLy,
    _diemHoa
  );
  return sv;
}

function renderDSSV(svArr) {
  let contentHTML = "";
  for (let i = 0; i < svArr.length; i++) {
    let item = svArr[i];
    let contentTr = `<tr>
            <td>${item.maSv}</td>
            <td>${item.hoTen}</td>
            <td>${item.email}</td>
            <td>${item.tinhDTB()}</td>
            <td>
            <button class="btn btn-danger" onclick= "xoaSinhVien('${
              item.maSv
            }')">Xóa</button>
            <button class="btn btn-info" onclick= "suaSinhVien('${
              item.maSv
            }')">Sửa</button>
            </td>
        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function duaThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSv
  document.getElementById("txtTenSV").value = sv.hoten
  document.getElementById("txtEmail").value = sv.email
  document.getElementById("txtPass").value = sv.matKhau
  document.getElementById("txtDiemToan").value = sv.diemToan
  document.getElementById("txtDiemLy").value = sv.diemLy
  document.getElementById("txtDiemHoa").value =sv.diemHoa
}

function timViTri(idSv, dssv) {
    let viTri = -1
    for(let i = 0; i<dssv.length;i++) {
        if(idSv == dssv[i].maSv) {
            viTri = i
        }
    }
    return viTri
}

