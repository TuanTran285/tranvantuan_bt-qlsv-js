function kiemTraId(maSv, svArr) {
    const index = svArr.findIndex((item) => {
        return maSv == item.maSv
    })
    if(index == -1) {
        document.getElementById("spanMaSV").innerHTML = ""
        return true
    }else {
        document.getElementById("spanMaSV").innerHTML = `<span class="text-danger">Mã sinh viên đã tồn tại</span>`
        return false
    }
}

function kiemTraSo(value, idErr) {
    var reg =  /^\d+(\.\d{1,2})?$/
    let outPut = reg.test(value)
    if(outPut) {
        document.getElementById(idErr).innerText = ""
        return true
    }else {
        document.getElementById(idErr).innerText = "Trường này phải là số"
        return false
    }
}

function kiemTraDoDai(value, idErr, min, max) {
    if(value.length < min || value.length > max) {
        document.getElementById(idErr).innerHTML = `Độ dài từ ${min} đến ${max} ký tự`
        return false
    }else {
        document.getElementById(idErr).innerHTML = ""
        return true
    }
}

function kiemTraEmail(email) {
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const value = re.test(email)
    if(value) {
        document.getElementById("spanEmailSV").innerHTML = ""
        return true
    }else {
        document.getElementById("spanEmailSV").innerHTML = "Trường này phải là Email"
        return false
    }
}

function kiemTraChu(value, idErr) {
    let regex = /[^0-9]/;
    let output = regex.test(value)
    if(output) {
        document.getElementById(idErr).innerHTML = ""
        return true
    }else {
        document.getElementById(idErr).innerHTML = "Trường này phải là chữ"
        return false
    }
}