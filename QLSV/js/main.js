let dssv = localStorage.length > 0 ? JSON.parse(localStorage.getItem("DSSV")) : []
// map qua để lấy được hàm function để tính điểm trùng bình
dssv = dssv.map(item => {
    return new SinhVien(item.maSv, item.hoTen, item.email, item.matKhau, item.diemToan, item.diemLy, item.diemHoa)
})
renderDSSV(dssv)


function themSinhVien() {
    const sv = layThongTinTuForm()

    let invalid = true
    // check id
    invalid = invalid && kiemTraId(sv.maSv, dssv) && kiemTraSo(sv.maSv, "spanMaSV") && kiemTraDoDai(sv.maSv, 'spanMaSV', 4, 6)

    // in tất cả các trường hợp nếu thông tin bị thiếu hoặc sai => &: cộng dồn bits
    // kiểm tra email
    invalid = invalid & kiemTraEmail(sv.email)
    // kiểm tra tên 
    invalid = invalid & kiemTraChu(sv.hoTen, "spanTenSV") & kiemTraDoDai(sv.matKhau, "spanMatKhau", 5, 20) & kiemTraSo(sv.diemToan, "spanToan") & kiemTraSo(sv.diemLy, "spanLy") & kiemTraSo(sv.diemHoa, "spanHoa")
    // kiểm tra mật khẩu
    invalid = invalid & kiemTraDoDai(sv.matKhau, "spanMatKhau", 5, 20)
    // kiểm tra điểm
    invalid = invalid & kiemTraSo(sv.diemToan, "spanToan") & kiemTraSo(sv.diemLy, "spanLy") & kiemTraSo(sv.diemHoa, "spanHoa")
    
    if(invalid) {
        console.log(dssv)
        dssv.push(sv)
        localStorage.setItem("DSSV", JSON.stringify(dssv))
        renderDSSV(dssv)
    }
    
}

function xoaSinhVien(idSv) {
    let viTri = timViTri(idSv, dssv)
    dssv.splice(viTri, 1)
    renderDSSV(dssv)
    localStorage.setItem("DSSV", JSON.stringify(dssv))
}

function suaSinhVien(idSv) {
    let viTri = timViTri(idSv, dssv)
    if(viTri == -1) {
        return
    }
    const index = dssv[viTri]
    duaThongTinLenForm(index)
}

function capNhatSinhVien() {
    let sv = layThongTinTuForm()
    let viTri = timViTri(sv.maSv, dssv)
    if(viTri == -1) {
        return 
    }
    let invalid = true
    // check id
    invalid = invalid && kiemTraSo(sv.maSv, "spanMaSV") && kiemTraDoDai(sv.maSv, 'spanMaSV', 4, 6)

    // in tất cả các trường hợp nếu thông tin bị thiếu hoặc sai => &: cộng dồn bits
    // kiểm tra email
    invalid = invalid & kiemTraEmail(sv.email)
    // kiểm tra tên 
    invalid = invalid & kiemTraChu(sv.hoTen, "spanTenSV") & kiemTraDoDai(sv.matKhau, "spanMatKhau", 5, 20) & kiemTraSo(sv.diemToan, "spanToan") & kiemTraSo(sv.diemLy, "spanLy") & kiemTraSo(sv.diemHoa, "spanHoa")
    // kiểm tra mật khẩu
    invalid = invalid & kiemTraDoDai(sv.matKhau, "spanMatKhau", 5, 20)
    // kiểm tra điểm
    invalid = invalid & kiemTraSo(sv.diemToan, "spanToan") & kiemTraSo(sv.diemLy, "spanLy") & kiemTraSo(sv.diemHoa, "spanHoa")

    if(invalid) {
        dssv[viTri] = sv
        renderDSSV(dssv)
        localStorage.setItem("DSSV", JSON.stringify(dssv))
        console.log("Succsess")

    }
}